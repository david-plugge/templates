import rollupConfig from './rollup.config';

/** @type {import('vitest/config').UserConfig} */
const config = {
	test: {
		// jest like globals
		globals: true
	},
	plugins: rollupConfig[0].plugins
};

export default config;
