import { add } from './method';

test('sub method', () => {
	expect(add(10, 5)).toBe(15);
	expect(add(5, 10)).toBe(15);
});
