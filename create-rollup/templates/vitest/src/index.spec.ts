import { sub } from './index';

test('sub method', () => {
	expect(sub(10, 5)).toBe(5);
	expect(sub(5, 10)).toBe(-5);
});
