import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
import pkg from './package.json';

const dev = process.env.ROLLUP_WATCH === 'true';
const external = [].concat(
	Object.keys(pkg.dependencies || {}),
	Object.keys(pkg.peerDependencies || {}),
	Object.keys(process.binding('natives'))
);

/** @type {import('rollup').RollupOptions[]} */
const config = [
	{
		input: 'src/index.ts',
		output: {
			file: 'dist/index.js',
			format: 'esm'
		},
		external: (id) => id.startsWith('node:') || external.includes(id),
		plugins: [
			resolve(),
			commonjs(),
			typescript({ tsconfig: './tsconfig.json' }),
			dev &&
				run({
					args: ['-r', 'source-map-support', '-r', 'dotenv/config']
				})
		]
	}
];

export default config;
