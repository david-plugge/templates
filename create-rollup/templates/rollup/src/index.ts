import { add } from './lib/method';

export function sub(a: number, b: number) {
	return add(a, -b);
}
