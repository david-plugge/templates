import fs from 'fs';
import prompts, { PromptObject } from 'prompts';
import { gray } from 'kleur/colors';
import { runners } from './options';
import { resolve } from './utils';
import { Context } from './types';

const { version, name } = JSON.parse(fs.readFileSync(resolve('package.json'), 'utf-8'));

function onCancel() {
	process.exit(1);
}

async function main() {
	console.log(gray(`\n${name} version ${version}\n`));

	let cwd = process.argv[2];

	if (!cwd) {
		const opts = await prompts([
			{
				type: 'text',
				name: 'dir',
				message: 'Where should we create your project?\n  (leave blank to use current directory)'
			}
		]);
		cwd = opts.dir ?? '.';
	}

	if (fs.existsSync(cwd)) {
		if (fs.readdirSync(cwd).length > 0) {
			const opts = await prompts([
				{
					type: 'confirm',
					name: 'value',
					message: 'Directory not empty. Continue?',
					initial: false
				}
			]);
			if (!opts.value) {
				process.exit(1);
			}
		}
	}

	const optionPropmts: PromptObject<keyof Context['options']>[] = [
		{
			type: 'toggle',
			name: 'types',
			message: 'Generate and bundle types?',
			initial: false,
			active: 'Yes',
			inactive: 'No'
		},
		{
			type: 'toggle',
			name: 'codeQuality',
			message: 'Add code quality tools (ESLint, prettier) ?',
			initial: false,
			active: 'Yes',
			inactive: 'No'
		},
		{
			type: 'toggle',
			name: 'vitest',
			message: 'Add Vitest for testing?',
			initial: false,
			active: 'Yes',
			inactive: 'No'
		},
		{
			type: 'toggle',
			name: 'docker',
			message: 'Add Dockerfile?',
			initial: false,
			active: 'Yes',
			inactive: 'No'
		}
	];

	const options = await prompts(optionPropmts, { onCancel });

	const context: Context = {
		cwd,
		options
	};

	for (const run of runners) {
		if (!run.filter(context)) continue;

		await run.setup(context);
	}
}

main();
