import deepmerge from 'deepmerge';
import { fileURLToPath } from 'url';
import path from 'path';
import fs from 'fs';

export function resolve(path: string) {
	return fileURLToPath(new URL(`./${path}`, import.meta.url).href);
}

export function mkdirp(dir: string) {
	try {
		fs.mkdirSync(dir, { recursive: true });
	} catch (e: unknown) {
		if ((e as { code: string }).code === 'EEXIST') return;
		throw e;
	}
}

export function extractTemplate(template: string, cwd: string) {
	const templatePath = resolve(template);
	copy(templatePath, cwd, (name) => name.replace('DOT-', '.'));
}

export function mergeJson(from: string, to: string) {
	if (!fs.existsSync(to)) {
		fs.copyFileSync(from, to);
		return;
	}
	const pkgFrom = JSON.parse(fs.readFileSync(from, 'utf-8'));
	const pkgTo = JSON.parse(fs.readFileSync(to, 'utf-8'));

	const pkg = deepmerge(pkgTo, pkgFrom);
	fs.writeFileSync(to, JSON.stringify(pkg, null, 4));
}

export function copy(from: string, to: string, rename = (x: string) => x) {
	if (!fs.existsSync(from)) return;

	const stats = fs.statSync(from);

	if (stats.isDirectory()) {
		fs.readdirSync(from).forEach((file) => {
			copy(path.join(from, file), path.join(to, file), rename);
		});
	} else {
		if (from.endsWith('package.json') || from.endsWith('tsconfig.json')) {
			mergeJson(from, to);
			return;
		}
		mkdirp(path.dirname(to));
		fs.copyFileSync(from, rename(to));
	}
}
