export interface Runner {
	filter: (options: Context) => boolean;
	setup: (options: Context) => Promise<void> | void;
}

interface Options {
	types: boolean;
	codeQuality: boolean;
	vitest: boolean;
	docker: boolean;
}

export interface Context {
	cwd: string;
	options: Options;
}
