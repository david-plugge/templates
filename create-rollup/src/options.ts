import { Runner } from './types';
import { extractTemplate } from './utils';

export const runners: Runner[] = [
	{ filter: () => true, setup: (context) => extractTemplate('templates/rollup', context.cwd) },
	{
		filter: ({ options }) => options.types,
		setup: (context) => extractTemplate('templates/types', context.cwd)
	},
	{
		filter: ({ options }) => options.codeQuality,
		setup: (context) => extractTemplate('templates/code-quality', context.cwd)
	},
	{
		filter: ({ options }) => options.vitest,
		setup: (context) => extractTemplate('templates/vitest', context.cwd)
	},
	{
		filter: ({ options }) => options.docker,
		setup: (context) => extractTemplate('templates/docker', context.cwd)
	}
];
