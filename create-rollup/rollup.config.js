import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import typescript from '@rollup/plugin-typescript';
import pkg from './package.json';

/** @type {import('rollup').RollupOptions} */
const config = {
	input: 'src/cli.ts',
	output: {
		file: 'bin.js',
		format: 'esm',
		banner: '#!/usr/bin/env node'
	},
	external: Object.keys(pkg.dependencies ?? {}),
	plugins: [resolve(), commonjs(), typescript({ tsconfig: './tsconfig.json' })]
};

export default config;
